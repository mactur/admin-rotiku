package rasyidk.fa.rotikuadmin.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import rasyidk.fa.rotikuadmin.R;
import rasyidk.fa.rotikuadmin.model.RotiItem;

public class ProdukAdapter extends RecyclerView.Adapter<ProdukAdapter.MyViewHolder> {
    private ArrayList<RotiItem> rotiItems;
    private Context context;

    public ProdukAdapter (Context context, ArrayList<RotiItem> rotiItems) {
        this.context = context;
        this.rotiItems = rotiItems;
    }

    private ArrayList<RotiItem> getRoti () {
        return rotiItems;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_produks, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        //Log.d("cek", String.valueOf(getRoti().get(position).getNama()));
        holder.txt_roti.setText(getRoti().get(position).getNama());
        Glide.with(context)
                .load(getRoti().get(position).getGambar())
                .apply(new RequestOptions().placeholder(R.drawable.ic_image_black_24dp).override(350, 550))
                .into(holder.img_roti);
    }

    @Override
    public int getItemCount() {
        return rotiItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_roti;
        TextView txt_roti;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_roti = itemView.findViewById(R.id.img_produk);
            txt_roti = itemView.findViewById(R.id.txt_roti);
        }
    }
}
