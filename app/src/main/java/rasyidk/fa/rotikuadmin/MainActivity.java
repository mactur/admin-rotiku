package rasyidk.fa.rotikuadmin;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

import rasyidk.fa.rotikuadmin.fragment.ProdukFragment;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mToogle;
    private NavigationView navigationView;
    private static FirebaseDatabase mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.drawerlayout);
        navigationView = findViewById(R.id.navigation_view);

        mToogle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(mToogle);
        mToogle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initFragment(new ProdukFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.dm_produk:
                        //Toast.makeText(, item.getTitle()+" pressed", Toast.LENGTH_SHORT).show();
                        Log.d("klik", "berhasil");
                        //Snackbar.make(this, item.getTitle()+" pressed", Snackbar.LENGTH_LONG).show();
                        item.setChecked(true);

                        drawerLayout.closeDrawers();
                        break;
                    case R.id.dm_karyawan:
                        break;
                    case R.id.dm_laporan:
                        break;
                    case R.id.dm_pengeluaran:
                        break;
                    case R.id.dm_logout:
                        break;
                }
                return false;
            }
        });

        if (mToogle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static FirebaseDatabase getDatabase() {
        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
            mDatabase.setPersistenceEnabled(true);
        }
        return mDatabase;
    }

    private void initFragment(Fragment classFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flContent, classFragment);
        transaction.commit();
    }
}
